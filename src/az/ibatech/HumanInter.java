package az.ibatech;

public interface HumanInter {
    void greetPet();

    void describePet();

    boolean feedPet(int generateNum);
}
